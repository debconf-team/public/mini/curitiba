---
title: Inscrição
---

# Inscrição

A inscrição para a MiniDebConf Curitiba 2018 é totalmente gratuita.

Se você precisar de certificado de participação, veja
[esta página](../certificates).

Para se inscrever, acesse e preencha esse formulário:

<http://pesquisa.softwarelivre.org/index.php/391289>

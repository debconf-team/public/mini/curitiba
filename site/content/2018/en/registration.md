---
title: Registration
---

# Registration

The registration for attending MiniDebConf Curitiba 2018 is free.

Se you need a certificate of participation, see the
[corresponding page](../certificates/).

To register, access and fill in this form (the first field is a language
selector, switch to English and the rest of the form will be in English):

<http://pesquisa.softwarelivre.org/index.php/391289>

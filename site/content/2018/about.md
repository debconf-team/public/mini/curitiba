---
title: Sobre
---

# Sobre a MiniDebConf Curitiba 2018

De 11 a 14 de abril (quarta, quinta, sexta e sábado) Curitiba sediará pelo
terceiro ano consecutivo uma MiniDebConf composta por palestras, oficinas,
*sprints*, *BSP (Bug Squashing Party)* e eventos sociais.

A MiniDebConf Curitiba 2018 é um evento aberto a todos(as), independente do seu
nível de conhecimento sobre Debian. O mais importante será reunir novamente a
comunidade para celebrar o maior projeto de Software Livre no mundo, por isso
queremos receber desde usuários(as) inexperientes que estão iniciando o seu
contato com o Debian até Desenvolvedores(as) oficiais do projeto. Ou seja, estão
todos(as) convidados(as)!

MiniDebConfs são encontros locais organizados por membros do Projeto Debian para
atingir objetivos semelhantes aos da [DebConf](http://debconf.org), mas em um
contexto regional. Durante todo o ano são organizadas MiniDebConfs ao redor do
mundo, como se pode ver [nesta página](https://wiki.debian.org/MiniDebConf).

Esta será a terceira vez que uma MiniDebConf nesses moldes, com quatro dias
dedicados exclusivamente ao Debian, acontecerá no Brasil.

## Data: 11 a 14 de abril de 2018

* **11 e 12 (quarta e quinta): MiniDebCamp** — período para colaboradores(as) do
  Debian se encontrarem e trabalharem conjuntamente em um ou mais aspectos do
  projeto.  Essa será a nossa versão da brasileira da
  [Debcamp](https://wiki.debconf.org/wiki/DebConf17/DebCamp), que acontece
  tradicionalmente antes da Debconf. Não haverá palestras, debates e oficinas
  nesses dias, teremos apenas "mão na massa" como empacotamentos de softwares,
  traduções e BSP - Bug Squashing Party :-)

* **13 e 14 (sexta e sábado): MiniDebConf** propriamente dita — palestras,
  debates, oficinas, e mais "mão na massa" :-D

* **15 (domingo)** — churrasco de confraternização (almoco) no salão de festas
  da Associação dos Professores da UFPR.

## Edições anteriores

Em 2017 aconteceu um evento dedicado ao Debian, a segunda edição da MiniDebConf
Curitiba:

* [MiniDebConf Curitiba](http://br2017.mini.debconf.org): 17 a 19/03/2017 -
  [fotos](https://www.flickr.com/photos/curitibalivre/albums/72157679698906961),
  [arquivos dos vídeos das palestras](http://ftp.acc.umu.se/pub/debian-meetings/2017/mini-debconf-curitiba) e
  [vídeos das palestras no youtube](https://www.youtube.com/watch?v=48jVIX6sgOo&list=PLU90bw3OpxLoKKXpGVpGzEiITDDnHzgAu)


Em 2016 aconteceram dois eventos dedicados ao Debian, sendo um deles a primeira
edição da MiniDebConf Curitiba:

* [MiniDebConf Curitiba](http://br2016.mini.debconf.org): 05 e 06/03/2016 -
  [fotos](http://softwarelivre.org/debianbrasil/blog/fotos-da-mini-debconf-curitiba-2016)
* [MiniDebConf na 13ª Latinoware](https://wiki.debian.org/Brasil/Eventos/MiniDebConfLatinoware2016): 19 e 20/10/2016 -
  [fotos](http://debianbrasil.org.br/fotos-na-latinoware-2016)

E em 2015 foram três eventos menores:

* [MicroDebConf em Brasília](https://wiki.debian.org/Brasil/Eventos/MicroDebConfBSB2015): 31/05/2015
* [MiniDebConf no FISL16](https://wiki.debian.org/Brasil/Eventos/MiniDebConfFISL2015):
  10/07/2015 - [fotos](https://www.flickr.com/photos/curitibalivre/sets/72157653569094123),
  [arquivos dos vídeos das palestras](http://ftp.acc.umu.se/pub/debian-meetings/2015/mini-debconf-fisl/) e
  [vídeos das palestras no youtube](https://www.youtube.com/watch?v=zZ0B8lgtdkc&list=PLU90bw3OpxLrzqwZmzu3CrWum5ToqF3DO).
* [MiniDebConf na 12ª Latinoware](https://wiki.debian.org/Brasil/Eventos/MiniDebConfLatinoware2015): 15/10/2015

Em 2004 o Brasil sediou em Porto Alegre a [5ª edição da DebConf](http://debconf4.debconf.org).

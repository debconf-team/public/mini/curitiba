---
title: Organizadores
---

# Organizadores

A MiniDebConf Curitiba 2018 é organizada pelos voluntários abaixo:

* Adriana Cássia da Costa
* Alisson Coelho
* Angelo Rosa
* Antonio C. C. Marques
* Antonio Terceiro
* Cleber Ianes
* Daniel Lenharo de Souza
* Gilmar Nascimento
* Giovani Ferreira
* Leonardo Rodrigues
* Lucas Kanashiro
* Paulo Henrique de Lima Santana
* Samuel Henrique
* Valéssio Brito

Contamos com a ajuda dos professores e funcionários da UTFPR abaixo:

* Prof. Adolfo Gustavo Serra Seca Neto (Departamento Acadêmico de Informática)
* Profa. Maria Claudia F. Pereira Emer (Departamento Acadêmico de Informática)
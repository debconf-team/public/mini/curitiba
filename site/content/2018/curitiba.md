---
title: Curitiba
---

# Hospedagem

Próximo a UTFPR existem vários hotéis que dá para ir a pé.
Fizemos um levantamento de preços no início de dezembro de 2016, então os
valores provavelmente estão maiores agora.

[Nacional Inn](http://www.nacionalinn.com.br/curitiba/nacional-inn-curitiba)

* Endereço: Rua Lourenço Pinto, 458
* É o mais próximo da UTFPR
* Quartos single: R$ 159,00
* Quartos duplo: R$ 189,00
* Quartos triplo: R$ 239,00

[San Juan Executive](https://www.sanjuanhoteis.com.br/pt-br/hoteis/detalhes/1/san-juan-executive)

* Endereço: Av. Sete de Setembro, 2516
* Quartos single: R$ 165,00
* Quartos duplo: R$ 195,00
* Quartos triplo: R$ 245,00

[Victoria Villa Hotel](http://victoriavillahotel.com.br)

* Endereço: Av. Sete de Setembro, 2448
* Quartos single: R$ 221,00
* Quartos duplo: R$ 271,00
* Quartos triplo:R$ 321,00

[Lizon Curitiba](http://www.lizon.com.br)

* Endereço: Av. Sete de Setembro, 2246
* Quartos single: R$ 159,00
* Quartos duplo: R$ 189,00
* Quartos triplo: R$ 259,00
* Quarto quádruplo: R$ 289,00

[Nacional Inn Torres](http://www.nacionalinn.com.br/curitiba/nacional-inn-torres)

* Endereço: R. Mariano Torres, 976
* Quartos single: R$ 159,00
* Quartos duplo: R$ 199,00
* Quartos triplo: R$ 249,00

Ibis Budget

* Endereço: R. Mariano Torres, 927
* Quartos single: R$ 145,00
* Quartos duplo: R$ 145,00

Go Inn

* Endereço: Rua Des. Motta, 1221
* Quartos single: R$ 198,00
* Quartos duplo: R$ 206,00

# Alimentação

O Campus central da UTFPR fica em frente ao 
Shopping Estação](http://www.shoppingestacao.com.br). No Shopping tem várias
opções na [praça de alimentação](http://www.shoppingestacao.com.br/Restaurante).

Na região em volta da UTFPR também tem vários restaurantes e lanchonetes
porque é uma área comercial. Então é possível encontrar restaurantes de todos
os preços.

# O que fazer em Curitiba

## Informações para turistas

* [Ônibus Linha Turismo](http://www.curitiba.pr.gov.br/idioma/portugues/linhaturismo):
  circula por vários pontos turísticos da cidade. A passagem custa R$ 45,00 e
  dá direito a descer em 4 locais e reembarcar.
* [Curitiba Turismo](http://www.turismo.curitiba.pr.gov.br)
* [Wikivoyage sobre Curitiba](https://en.wikivoyage.org/wiki/Curitiba)

## Lugares para visitar

A UTFPR fica próxima de dois pontos turísticos bastante conhecidos:

### 1 - Mercado Municipal de Curitiba

Fica a 1,3km e dá para ir a pé, ou pegar o ônibus bi-articulado em frente ao
Shopping Estação descer 2 estações depois. Esse ônibus custa R$ 4,25.

[Mais informações](http://mercadomunicipaldecuritiba.com.br)

### 2 - Jardim Botânico

Fica a 3,5km e você pode pegar ônibus bi-articulado em frente ao Shopping
Estação e descer em frente do Jardim Botânico. Você deve levar de 10 a 15 min
para chegar lá. Esse ônibus custa R$ 4,25.

[Mais informações](http://www.turismo.curitiba.pr.gov.br/conteudo/jardim-botanico/1674)

### Feira do Lago da Orgem

Outro local que vale muito a pena visitar é a Feira do Lago da Ordem. Ela
funciona apenas aos domingos pela manhã e é o melhor lugar para comprar
artesanato e lembranças de Curitiba. Não deixe de ir lá!

[Vídeo](https://www.youtube.com/watch?v=0B_b1TTJLyw)

[Mais informações](http://www.turismo.curitiba.pr.gov.br/conteudo/feira-do-largo-da-ordem/1027)

### Outros locais para visitar:

Você pode ver a lista completa de locais para visitar
[nesse site](http://www.turismo.curitiba.pr.gov.br)

Algumas recomendações

* Prédio histórico da UFPR (fica no centro)
* Lago da Ordem (fica no centro)
* Museu Oscar Niemeyer (fica próximo ao centro)
* Ópera de Arame
* Torre Panorâmica
* Parque Tanguá
* Parque Barigui












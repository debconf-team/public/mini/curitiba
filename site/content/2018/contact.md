---
title: Contato
---

# Contato

Para entrar em contato com a organização da MiniDebConf Curitiba 2018:

* E-mail: <brasil.mini@debconf.org>
* Lista pública de discussão: <debian-br-eventos@lists.alioth.debian.org>
* Canal IRC: #debian-br-eventos no irc.debian.org

Redes sociais livres e descentralizadas:

* [Debian Brasil GNU Social/StatusNet](https://quitter.se/debianbrasil)
* [Debian Brasil no Pump.io](https://identi.ca/debianbrasil)
* [Debian Brasil no Hubzilla](https://hub.vilarejo.pro.br/channel/debianbrasil)

Redes sociais proprietárias e centralizadas:

* [Debian Brasil no Telegram](https://t.me/debianbrasil)
* [Debian Brasil no Twitter](https://twitter.com/debianbrasil)
* [Debian Brasil no Facebook](https://www.facebook.com/DebianBrasil/)
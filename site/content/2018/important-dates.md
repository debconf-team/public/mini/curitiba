---
title: Datas importantes
---

# Datas importantes

## Janeiro de 2018

* 31 (quarta) 23h59min: Prazo final para submissão de
  [atividades](../call-for-proposals).

## Fevereiro de 2018

* 11 (domingo): Divulgação das atividades aprovadas.

## Março de 2018

* 10 (sábado): Encontro Debian Women em Curitiba.

## Abril de 2018

* 11 e 12 (quarta e quinta): MiniDebCamp 2018 Curitiba.
* 13 e 14 (sexta e sábado): MiniDebConf 2018 Curitiba.
* 15 (domingo): almoço de confraternização.
